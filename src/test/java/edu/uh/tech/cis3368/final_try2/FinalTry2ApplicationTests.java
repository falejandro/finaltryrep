package edu.uh.tech.cis3368.final_try2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FinalTry2ApplicationTests {

    @Test
    public void contextLoads() {
    }

}
