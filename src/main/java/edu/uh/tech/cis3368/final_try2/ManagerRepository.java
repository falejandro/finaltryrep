package edu.uh.tech.cis3368.final_try2;

import org.springframework.data.repository.CrudRepository;

public interface ManagerRepository extends CrudRepository<ManagerEntity, Integer> {
}
