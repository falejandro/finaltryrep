package edu.uh.tech.cis3368.final_try2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalTry2Application implements CommandLineRunner {

    @Autowired
    ManagerRepository managerRepository;
    public static void main(String[] args) {
        SpringApplication.run(FinalTry2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ManagerEntity managerEntity = new ManagerEntity();
        managerEntity.setFirstName("Keith");
        managerEntity.setLastName("Lancaster");
        managerRepository.save(managerEntity);

        Iterable<ManagerEntity> iterable = managerRepository.findAll();
        for(ManagerEntity managerEntity1 : iterable){
            System.out.println(managerEntity1);
            System.out.println("hello");
        }
    }
}
