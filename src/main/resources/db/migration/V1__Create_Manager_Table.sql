CREATE TABLE MANAGER
  (
    id int auto_increment PRIMARY KEY,
    last_name varchar(24),
    first_name varchar(24)
  );